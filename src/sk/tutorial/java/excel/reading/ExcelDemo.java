package sk.tutorial.java.excel.reading;

import java.io.File;

public class ExcelDemo {
    public static void main(String[] args) {
        File excelFile = new File("data.xlsx");

        if(excelFile.exists()){
            ExcelUtils.readExcelFile(excelFile);
        }
        else{
            System.out.println("File does not exist!");
        }
    }
}

/* USED JARS
* 
* ooxml-schemas-1.1.jar
* poi-3.13.jar
* poi-ooxml-3.13.jar
* xmlbeans-2.6.0.jar
* */

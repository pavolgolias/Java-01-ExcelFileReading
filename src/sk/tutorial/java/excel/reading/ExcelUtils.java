package sk.tutorial.java.excel.reading;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

public class ExcelUtils {
    public static void readExcelFile(File excelFile){
        try{
            FileInputStream inputStream = new FileInputStream(excelFile);
            Workbook workbook = new XSSFWorkbook(inputStream);  // Use XSSF for xlsx format, for xls use HSSF
            Sheet firstSheet = workbook.getSheetAt(0);          // Get the first sheet

            String tmpResult = "";
            for (Row row : firstSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    tmpResult += String.valueOf(getCellValue(cell)) + " - ";
                }

                System.out.println();
                System.out.println("Row loaded: " + tmpResult);
                tmpResult = "";
            }
        } catch (Exception e){
            System.err.println("Error while reading Excel file, " + e);
        }
    }

    private static Object getCellValue(Cell cell){
        switch (cell.getCellType()){
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            case Cell.CELL_TYPE_NUMERIC:
                return cell.getNumericCellValue();
            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();
        }

        return null;
    }
}
